<?php

class Auth_model extends CI_Model
{
    public function get_data()
    {
        $this->db->get('user')->result_array();
        $data = [
            'name' => htmlspecialchars($this->input->post('name', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'image' => 'default.jpg',
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'role_id' => 2,
            'is_active' => 1,
            'date_created' => time()
        ];
        $this->db->insert('user', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat! Akun anda sudah terdaftar. Silahkan Login!
          </div>');
        redirect('auth');
    }
}
